from django.shortcuts import render
from .models import Database

# Create your views here.

expertise = ['Marketing', 'Collector', 'Public Speaking']
profile_name = 'Audrey Hepburn'
birthdate = '01 Jan'
gender = 'Female'
email = 'hello@smith.com'
desc_profile = 'Experience as marketer for 10 years'
response = {}

def index(request):
    response['name'] = profile_name
    response['birthday'] = birthdate
    response['gender'] = gender
    response['expertise'] = expertise
    response['description'] = desc_profile
    response['email'] = email
    return render(request, 'halaman-profile.html', response)