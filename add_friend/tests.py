from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, message_post, message_table, remove_contact
from .forms import Message_Form
from .models import Message

# Create your tests here.
class AddFriendUnitTest(TestCase):
    def test_add_friend_url_is_exist(self):
        response = Client().get('/add_friend/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add_friend/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_message(self):
        #Creating a new activity
        new_activity = Message.objects.create(name='kapewe',link='kapewe.herokuapp.com',)
        #Retrieving all available activity
        counting_all_available_message= Message.objects.all().count()
        self.assertEqual(counting_all_available_message,1)

    def test_add_friend_post_fail(self):
        response = Client().get('/add_friend/')
        self.assertEqual(response.status_code, 200)


    def test_add_friend_post_success_and_render_the_result(self):
        anonymous = 'Anonymous'
        message = 'Anonymous'
        response = Client().post('/add_friend/add_message/', {'name': '', 'link': ''}, follow=True)
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn(anonymous,html_response)
        self.assertIn(message,html_response)

    def test_add_message_table_using_message_table_func(self):
        found = resolve('/add_friend/result_table')
        self.assertEqual(found.func, message_table)

    def test_add_friend_showing_all_messages(self):
        name_kapewe = 'kapewe'
        link_kapewe = 'kapewe.herokuapp.com'
        data_kapewe = {'name': name_kapewe, 'link': link_kapewe}
        post_data_kapewe = Client().post('/add_friend/add_message', data_kapewe)
        self.assertEqual(post_data_kapewe.status_code, 302)

        data_anonymous = {'name': '', 'link': ''}
        post_data_anonymous = Client().post('/add_friend/', data_anonymous)
        self.assertEqual(post_data_anonymous.status_code, 200)

        response = Client().get('/add_friend/',follow = True)
        html_response = response.content.decode('utf8')

        for key,data in data_kapewe.items():
            self.assertIn(data,html_response)

        self.assertIn(name_kapewe, html_response)

    def test_friends_remove_contact(self):
        new_contact = Message.objects.create(name="pewepewe", link="https://pewewe.herokuapp.com/")
        object = Message.objects.all()[0]
        response_post = Client().post('/add_friend/remove_contact', {'flag': str(object.id)})

        response= Client().get('/add_friend/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('<form id="form-' + str(object.id) + '', html_response)
