from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
#from .forms import Add_Friend_Form
#from .models import Friend

# Create your views here.
response = {}
def index(request):
    response['author'] = "Luqman Iffan Windrawan"
    message = Message.objects.all()#.values()
    #response["Add_Friend_Form"] = Add_Friend_Form
    response["message"] = message
    html = 'addfriend.html'
    return render(request, html, response)

def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['link'] = request.POST['link'] if request.POST['link'] != "" else "Anonymous"
        message = Message(name=response['name'], link=response['link'])
        message.save()
        html ='addfriend.html'
        #render(request, html, response)
        return HttpResponseRedirect('/add_friend/')
    # else:        
    #     return HttpResponseRedirect('/add_friend/')

def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'addfriend.html'
    return render(request, html, response)

def remove_contact(request):
    try:
        idObj = request.POST['flag']
        Message.objects.filter(id=idObj).delete()
    except ValueError:
        pass
    except KeyError:
        pass
    return HttpResponseRedirect('/add_friend/')
