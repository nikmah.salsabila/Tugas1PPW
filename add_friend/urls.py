from django.conf.urls import url
from .views import index, message_post, message_table, remove_contact
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_message', message_post, name='add_message'),
    url(r'^result_table', message_table, name='result_table'),
    url(r'^remove_contact', remove_contact, name='remove_contact')
    # url(r'^add_friend', add_friend, name='add_friend'),
    # url(r'^friend_table', friende_table, name='friend_table'),
]
