from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan alamat heroku',
    }
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama', required=True, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    heroku = forms.URLField(required=True, widget=forms.URLInput(attrs=attrs), empty_value='unknown.herokuapp.com')

