from django.test import Client
from django.urls import resolve
from django.test import TestCase
from .views import index
# Create your tests here.
class StatsUnitTest(TestCase):
    def test_dashboard_navigation_footer_url_is_exist(self):
        response = Client().get('/dashboard_navigation_footer/')
        self.assertEqual(response.status_code, 200)

    def test_dashboard_navigation_footer_using_index_func(self):
        found = resolve('/dashboard_navigation_footer/')
        self.assertEqual(found.func, index)
