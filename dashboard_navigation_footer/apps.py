from django.apps import AppConfig


class DashboardNavigationFooterConfig(AppConfig):
    name = 'dashboard_navigation_footer'
